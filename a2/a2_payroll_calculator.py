import math

def get_requirements():
    print("Program Requirements:")
    print("1. Must use float data type for user input.")
    print("2. Overtime rate: 1.5 times hourly rate (hours over 40).")
    print("3. Holiday rate: 2.0 times hourly rate (all holiday hours).")
    print("4. Must format currency with dollar sign, and round to two decimal places.")
    print("Create at least three functions that are called by the program:")
    print("\t main(): calls at least two other functions.")
    print("\t get_requirements(): displays the program requirements.")
    print("\t calculate_payroll(): calculates an individual one-week paycheck.")

def calculate_payroll(hours_worked, hours_holiday, pay):
    hours_overtime = hours_worked - 40

    base = (hours_worked - hours_overtime) * pay
    overtime = hours_overtime * (pay * 1.5)
    holiday = hours_holiday * (pay * 2)
    gross = base + overtime + holiday

    print("\nOutput:")
    print("Base:        ${:,.2f}".format(base))
    print("Overtime:    ${:,.2f}".format(overtime))
    print("Holiday:     ${:,.2f}".format(holiday))
    print("Gross:       ${:,.2f}".format(gross))
    
def main():
    get_requirements()

    print ("\nInput:")
    hours_worked = float(input("Enter hours worked: "))
    hours_holiday = float(input("Enter holiday hours: ")) 
    pay = float(input("Enter hourly pay rate: "))
    
    calculate_payroll(hours_worked, hours_holiday, pay)

print("Payroll Calculator \n")

main()
