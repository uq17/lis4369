> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Umar Qureshi

### Assignment 2 Requirements:

*Two Parts:*

1. Develop Python program to calculate payroll
2. Chapter Questions

#### README.md file should include the following items:

* Screenshots of *a2_payroll_calculator* application running; 

#### Assignment Screenshots:

*Screenshots of *a2_payroll_calculator* application running (IDLE):

![IDLE Screenshot](img/idle1.png)
![IDLE Screenshot](img/idle2.png)

*Screenshots of *a2_payroll_calculator* application running (Visual Studio Code):

![Visual Studios Screenshot](img/visualstudio1.png)
![Visual Studios Screenshot](img/visualstudio2.png)

