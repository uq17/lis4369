installed.packages()
update.packages()

data()
mtcars

mydata <- read.csv("http://bit.ly/10ER84j")
pew_data <- read.csv("http://bit.ly/11I3iuU")

install.packages('quantmod')
library('quantmod')
getSymbols("AAPL")
barChart(AAPL)

barChart(AAPL, subset='last 14 days')

chartSeries(AAPL, subset='last 14 days')

barChart(AAPL['2013-04-01::2013-04-12'])

rm(x)

save.image()

save(variablename, file="filename.rda")
load("filename.rda")

head(mydata)
head(mydata, n=10)
head(mydata, 10)
tail(mydata)
tail(mydata, 10)
str(mydata)
colnames(mydata)

rownames(mydata)
summary(mydata)

install.packages("psych")
library(psych)
describe(mydata)

cor(mydata)

mean(myvector, na.rm=TRUE)
?median

choose(15,4)
mypeople <- c("Bob", "Joanne","Sally", "Tim", "Neal")

combn(mypeople, 2)
combn(c("Bob", "Joanne", "Sally","Tim", "Neal"),2)

names(mtcars)
mtcars.colnames <- names(mtcars)
mtcars$mpg
mtcars[,2:4]
mtcars[,c(2,4)]
mtcars[,c(2,4)]
mtcars$mpg>20

mtcars[mtcars$mpg>20,]
mtcars[mtcars$mpg>20,c(1,4)]
mtcars[mtcars$mpg>20,c("mpg","hp")]
mpg20 <- mtcars$mpg > 20
cols <- c("mpg", "hp")
mtcars[mpg20, cols]

attach(mtcars)
mpg20 <- mtcars$mpg > 20
mpg20 <- mpg > 20
detach()

subset(mtcars, mpg>20, c("mpg","hp"))

subset(mtcars, mpg==max(mpg))
subset(mtcars, mpg==max(mpg), mpg)
subset(mtcars, , c("mpg", "hp"))
subset(mtcars, select=c("mpg", "hp"))
filter(mtcars, mpg>20)
select(mtcars, mpg, hp)

mtcars %.% filter(mpg > 20) %.% select(mpg, hp)
mtcars %.% filter(mpg > 20) %.% select(mpg, hp) %.% arrange(desc(mpg))

table(diamonds$cut)
table(diamonds$cut, diamonds$color)

plot(mtcars$disp, mtcars$mpg)
plot(mtcars$disp, mtcars$mpg,xlab="Engine displacement",ylab="mpg", main="MPG compared with engine displacement")
plot(mtcars$disp, mtcars$mpg,xlab="Engine displacement",ylab="mpg", main="MPG vs engine displacement",las=1)
?par

install.packages("ggplot2")
library(ggplot2)
qplot(disp, mpg, data=mtcars)
qplot(disp, mpg, ylim=c(0,35),data=mtcars)

qplot(cty, hwy, data=mpg)
qplot(cty, hwy, data=mpg,geom="jitter")
ggplot(mtcars, aes(x=disp, y=mpg)) + geom_point()
ggplot(pressure, aes(x=temperature,y=pressure)) + geom_line()

ggplot(mydata, aes(x=xcol, y=ycol),ylim=0) + geom_line()
ggplot(pressure, aes(x=temperature,y=pressure)) + geom_line() + geom_point()

barplot(BOD$demand)
barplot(BOD$demand, main="Graph of demand")

barplot(BOD$demand, main="Graph of demand", names.arg = BOD$Time)
cylcount <- table(mtcars$cyl)

barplot(cylcount)
qplot(mtcars$cyl)

qplot(factor(mtcars$cyl))
ggplot(mtcars, aes(factor(cyl))) + geom_bar()

hist(mydata$columnName, breaks = n)
qplot(columnName, data=mydata,binwidth=n)
ggplot(mydata, aes(x=columnName)) + geom_histogram(binwidth=n)
boxplot(mtcars$mpg)

boxplot(diamonds$x, diamonds$y,diamonds$z)

rainbow(5)
?rainbow

mycolors <- rainbow(3)
mycolors <- heat.colors(3)

ggplot(mtcars, aes(x=factor(cyl))) + geom_bar(fill=mycolors)
ggplot(mtcars, aes(x=factor(cyl))) + geom_bar(fill=rainbow(3))

barplot(BOD$demand, col=rainbow(6))
barplot(BOD$demand, col="royalblue3")
testscores <- c(96, 71, 85, 92, 82,78, 72, 81, 68, 61, 78, 86, 90)
barplot(testscores)
barplot(testscores, col="blue")
testcolors <- ifelse(testscores >= 80, "blue", "red")

barplot(testscores, col=testcolors)
barplot(testscores, col=testcolors,main="Test scores")
barplot(testscores, col=testcolors,main="Test scores", ylim=c(0,100))
barplot(testscores, col=testcolors,main="Test scores", ylim=c(0,100),las=1)

testscores <- sort(c(96, 71, 85, 92,82, 78, 72, 81, 68, 61, 78, 86, 90),decreasing = TRUE)
testscores <- c(96, 71, 85, 92, 82,78, 72, 81, 68, 61, 78, 86, 90)
testscores_sorted <- sort(testscores,decreasing = TRUE)
ggplot(results, aes(x=students,y=testscores)) + geom_bar(fill=testcolors, stat ="identity")

qplot(factor(cyl), data=mtcars,geom="bar", fill=factor(cyl))

jpeg("myplot.jpg", width=350,height=420)
barplot(BOD$demand, col=rainbow(6))
dev.off()

x <- 3
my_vector <- c(1, 1, 2, 3, 5, 8)
my_vector <- (1:10)
my_vector <- c(1, 4, "hello", TRUE)
My_list <- list(1,4,"hello", TRUE)
my_vector <- c(7,9,23,5)
my_pct_vector <- my_vector * 0.01

apply(my_matrix, 1, median)
apply(my_matrix, 2, median)

class(3)
class(3.0)
class(3L)
class(as.integer(3))

dim(my_matrix)

sqldf("select * from mtcars where mpg > 20 order by mpg desc")
edit(mtcars)

write.table(mydata, "testfile.txt",sep="\t")