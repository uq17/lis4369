> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Umar Qureshi

### Assignment 5 Requirements:

*Four Parts:*

1. Code and run R file lis4369_a5.r, which displays R data manipulation and plots
2. Complete Introduction_to_R_Setup_and_Tutorial, which displays data manipulation and plots
3. Display workspace within R Studio for each file
4. Chapter Questions

#### README.md file should include the following items:

* Screenshots of *lis4369_a5.r* running in R Studio; 
* Screenshots of *tutorial.r* running in R Studio; 
* Screenshots of graphs/plots from both files

#### Assignment Screenshots:

* Screenshots of *lis4369_a5.r*:

* A5 in R Studio:

![Screenshot](img/rstudio1.PNG)

* A5 graphs/plots:

![Screenshot](img/Rplot.png)
![Screenshot](img/Rplot01.png)

* Screenshots of *tutorial.r*:

* Tutorial in R Studio:

![Screenshot](img/tutorial.PNG)

* Tutorial output:

![Screenshot](img/tutorial_output.PNG)

* Tutorial graphs/plots:

![Screenshot](img/tut1.png)
![Screenshot](img/tut2.png)
![Screenshot](img/tut3.png)
![Screenshot](img/tut4.png)
![Screenshot](img/tut5.png)