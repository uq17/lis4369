def get_requirements():
    print("Program Requirements:")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing installations.")
    print("3. Test Python Package Installer: pip freeze.")
    print("4. Research how to do the following installations:.")
    print("\t a. pandas (only if missing)")
    print("\t b. pandas-datareader (only if missing)")
    print("\t c. matplotlib (only if missing)")
    print("Create at least three functions that are called by the program:")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays the program requirements.")
    print("\t c. data_analysis_1(): displays the following data.")
    
def data_analysis_1():
    import pandas as pd
    import datetime
    import pandas_datareader as pdr
    import matplotlib.pyplot as plt
    from matplotlib import style

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("Print columns:")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("/nPrint first five lines: ")
    print(df.head())

    print("\nPrint last five lines: ")
    print(df.tail())

    print("Print first 2 lines:")
    print(df.head(2))

    print("Print last 2 lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()

