> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Umar Qureshi

### Assignment 3 Requirements:

*Two Parts:*

1. Develop Python program to calculate paint costs, with a iteration structure
2. Chapter Questions

#### README.md file should include the following items:

* Screenshots of *a2_paint_estimator_calculator* application running; 

#### Assignment Screenshots:

*Screenshots of *a2_paint_estimator_calculator* application running (IDLE):

![IDLE Screenshot](img/main.png)
![IDLE Screenshot](img/main2.png)

