import math

def get_requirements():
    print("Program Requirements:")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350).")
    print("4. Must use iteration structure (aka \"loop\").")
    print("5. Format, right-align numbers, and round to two decimal places.")
    print("6.Create at least three functions that are called by the program:")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays the program requirements.")
    print("\t c. calculate_payroll(): calculates an individual one-week paycheck.")

def estimate_painting_cost(sqft, price, rate):
    gallons = sqft/SQFT_PER_GALLON
    paint_cost = price * gallons
    labor_cost = sqft * rate
    total = paint_cost + labor_cost
    paint_per = (paint_cost/total) * 100
    labor_per = (labor_cost/total) * 100
    total_per = paint_per + labor_per

    print("\nOutput:")
    print("Item \t\t\tAmount")
    print("Total Sq Ft:{:>18,.2f}".format(sqft))
    print("Sq Ft per Gallon:{:>13,.2f}".format(SQFT_PER_GALLON))
    print("Number of Gallons:{:>12,.2f}".format(gallons))
    print("Price per Gallon:    ${:>8,.2f}".format(price))
    print("Labor per Sq Ft:     ${:>8,.2f}".format(rate))

    print("\nOutput:")
    print("Cost        Amount    Percentage")
    print("Paint:   ${:>8,.2f}".format(paint_cost), "  {:>10,.2f}%".format(paint_per))
    print("Labor:   ${:>8,.2f}".format(labor_cost) , "  {:>10,.2f}%".format(labor_per))
    print("Total:   ${:>8,.2f}".format(total) , "  {:>10,.2f}%".format(total_per))

    
def main():
    get_requirements()
    confirm = "y"
    while confirm == "y":
        print ("\nInput:")
        sqft = float(input("Enter total interior sq ft: "))
        price = float(input("Enter price per gallon paint: ")) 
        rate = float(input("Enter hourly painting rate per sq ft: "))
    
        estimate_painting_cost(sqft, price, rate)
        print("\n")
        confirm = input("Estimate another paint job? (y/n): ")

        if confirm == "n":
            print("\nThank you for using our Painting Estimator!")
            print("Please see our web site: http://www.mysite.com")

print("Painting Estimator \n")
SQFT_PER_GALLON = 350

main()
