> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Umar Qureshi

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and BitBucket 
2. Development Installations
3. Chapter Questions
4. Bitbucket repo links:
	a) this assignment and
	b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of *a1_tip_calculator* application running; 
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initializes a new Git repository
2. git status - displays the state of the working directory/snapshot
3. git add - moves changes from the working directory to the staging area
4. git commit - Commits staged snapshot to the project history
5. git push - moves a local branch to the remote repository
6. git pull - downloads a branch from the remote repository and merges it to the current
7. git branch - allows for isolated development environments within repository

#### Assignment Screenshots:

*Screenshot of *a1_tip_calculator* application running (IDLE):

![IDLE Screenshot](img/idle.png)

*Screenshot of *a1_tip_calculator* application running (Visual Studio Code):

![Visual Studios Screenshot](img/visualstudio.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/uq17/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/uq17/myteamquotes/ "My Team Quotes Tutorial")
