import math

print("Tip Calculator \n")

print("Program Requirements:")
print("1. Must use float data type for user input (except, \"Party Number\").")
print("2. Must round calculations to two decimal places.")
print("3. Must format cureency with dollar sign, and two decimal places.")

print ("\nUser Input:")

cost = float(input("Cost of meal: "))
tax = (float(input("Tax percent: ")))/100 
tip = (float(input("Tip percent: ")))/100
party = input("Party number: ")

taxdue = cost * tax
amountdue = cost + (cost * tax)
gratuity = amountdue * tip
total = amountdue + gratuity
split = total/(float(party))

print("\nProgram Output:")
print("Subtotal:         ${:,.2f}".format(cost))
print("Tax:              ${:,.2f}".format(taxdue))
print("Amount Due:       ${:,.2f}".format(amountdue))
print("Gratuity:         ${:,.2f}".format(gratuity))
print("Total:            ${:,.2f}".format(total))
print("Split (" + party + "):        ${:,.2f}".format(split))

