> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4369 -  Extensible Enterprise Solutions

## Umar Qureshi

### LIS 4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
	- Install R Studio
	- Install Visual Studio Code
	- Create *a1_tip_calculator* application)
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations
	- Provide git command descriptions
	
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Python program to make payroll calculations
	- Provide screenshots of program running

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create Python program to calculate painting costs, with iteration structure
	- Provide screenshots of program running

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Run python files that collects data into DataFrame
	- Install needed python packages if not already (pandas, pandas-datareader, matplotlib)
	- Add various print statements to display data in different ways and graphs it.

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Code and run files using R programming language
	- Manipulate and display data using R
	- Familiarize with and run code within R Studio

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Run two python files that collect stock data by webscraping
	- Install needed python packages if not already (pandas, pandas-datareader, matplotlib)
	- Add various print statements to display data in different columns

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Code and run R file that displays a dataset in various ways
	- Write this output to a .txt file using sink
	- Display png plot files created by R file
