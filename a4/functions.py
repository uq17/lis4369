def get_requirements():
    print("Program Requirements:")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing installations.")
    print("3. Test Python Package Installer: pip freeze.")
    print("4. Research how to install any missing packages.")
    print("5. Create at least three functions that are called by the program:")
    print("\t a. main(): calls at least two other functions.")
    print("\t b. get_requirements(): displays the program requirements.")
    print("\t c. data_analysis_2(): displays results as per demo.py.")
    print("6. Display graph as per instructions w/in demo.py.")
    
def data_analysis_2():
    import re
    import numpy as np
    np.set_printoptions(threshold=np.inf)
    import pandas as pd
    import matplotlib.pyplot as plt
    from matplotlib import style

    url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
    df = pd.read_csv(url)

    print("***DataFrame composed of three components: index, columns, and data. Data also known as values.***")

    index = df.index
    columns = df.columns
    values = df.values

    print("\n1. Print indexes:")
    print(index)

    print("\n2. Print columns:")
    print(columns)

    print("\n3. Print columns (another way):")
    print(df.columns[:])

    print("\n4. Print all values, in array format:")
    print(values)

    print("\n5. ***Print component data types:***")

    print("\na) Print index type:")
    print(type(index))

    print("\nb) Print columns type:")
    print(type(columns))

    print("\nc) Print values type:")
    print(type(values))

    print("\n6. Print summary of DataFrame (similar to describe tablename; in MySQL):")
    print(df.info())

    print("\n7. First five lines (all columns):")
    print(df.head())

    df = df.drop('Unnamed: 0', 1)
    print("\n8. Print summary of DataFrame (after dropping column 'Unnamed: 0'):")
    print(df.info())

    print("\n9. First five lines (after dropping 'Unnamed: 0'):")
    print(df.head())

    print("\n***Precise data slection (data slicing):***")
    print("\n10. Using iloc, return first 3 rows:")
    print(df.iloc[:3])

    print("\n11. Using iloc, return first 3 rows (start on index 1310 to end):")
    print(df.iloc[1310:])

    print("\n12. Select columns 1, 3, and 5 and rows 2, 4, and 6:")
    a = df.iloc[[0, 2, 4], [1, 3, 5]]
    print(a)

    print("\n13. Select all rows; and columns 1, 3, and 5:")
    a = df.iloc[:, [1, 3, 5]]
    print(a)

    print("\n14. Select rows 0, 2, 4; and all columns:")
    a = df.iloc[[0, 2, 4], :]
    print(a)

    print("\n15. Select all rows, and all columns, N:")
    a = df.iloc[:, :]
    print(a)

    print("\n16. Select all rows, and all columns, starting at column 2:")
    a = df.iloc[:, 1:]
    print(a)

    print("\n17. Select row 1, and column 1:")
    a = df.iloc[0:1, 0:1]
    print(a)

    print("\n18. Select rows 3-5, and columns 3-5:")
    a = df.iloc[2:5, 2:5]
    print(a)

    print("\n19. ***Convert pandas DataFrame df to NumPy ndarray, use values command:***:")
    b = df.iloc[:, 1:].values

    print("\n20. Print data frame type:")
    print(type(df))

    print("\n21. Print a type:")
    print(type(a))

    print("\n22. Print b type:")
    print(type(b))

    print("\n23. Print number of dimensions and items in array (rows, columns):")
    print(b.shape)

    print("\n24. Print type of items in array. This is an array of arrays:")
    print(b.dtype)

    print("\n25. Printing a:")
    print(a)

    print("\n26. Length a:")
    print(len(a))

    print("\n27. Printing b:")
    print(b)

    print("\n28. Length b:")
    print(len(b))

    print("\n29. Print element of (NumPy array) ndarray b in *second* row, *third* column:")
    print(b[1,2])

    print("\n30. Print all records for NumPy array column 2:")
    print(b[:, 1])

    print("\n31. Get passenger names:")
    names = df["Name"]
    print(names)

    print("\n32. Find all passengers with name 'Allison' (using regex):")
    for name in names:
        print(re.search(r'(Allison)', name))

    print("\n33. *** Statistical Analysis (DataFrame Notation)***:")
    print("\na) Print mean age:")
    avg = df["Age"].mean()
    print(avg)

    print("\nb) Print mean age, rounded to two decimal places:")
    avg = df["Age"].mean().round(2)
    print(avg)

    print("\nc) Print mean of every column in DataFrame:")
    avg_all = df.mean(axis=0)
    print(avg_all)

    print("\nd) Print summary statistics:")
    describe = df["Age"].describe()
    print(describe)

    print("\ne) Print minimum age:")
    min = df["Age"].min()
    print(min)

    print("\nf) Print maximum age:")
    max = df["Age"].max()
    print(max)

    print("\ng) Print median age:")
    median = df["Age"].median()
    print(median)

    print("\nh) Print mode age:")
    mode = df["Age"].mode()
    print(mode)

    print("\ni) Print number of values:")
    count = df["Age"].count()
    print(count)

    print("\n***Graph: Display ages of the first 20 passengers:***")
    style.use('ggplot')
    df['Age'].head(20).plot()
    plt.xlabel('Passenger Number')
    plt.legend()
    plt.show()












