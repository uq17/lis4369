# Data Visualization with Python

## Umar Qureshi

This project was developed using Python with the packages pandas, pandas-data-reader and matplotlib. 
The program is able to scrape data hosted on a website, in this case Yahoo Stocks, and display this data 
in various layouts through Python's API. Additionally, the program is able to output this scraped data as 
a customizable graph that can be saved for later use. View the project requirements and screenshots of the 
program below and download the source code [here](functions.py).

### Requirements:

1. Run demo file to display dataframe in various ways
2. Check for current python packages (pip freeze), install pandas, pandas-data-reader, and matplotlib if not installed
3. Add various print functions to display webscrape data in columns

#### Project Screenshots:

Screenshots of *main.py* application running (IDLE):

![IDLE Screenshot](img/1.PNG)
![IDLE Screenshot](img/2.PNG)
![IDLE Screenshot](img/graph1.PNG)