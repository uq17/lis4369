# Data Visualization with R

## Umar Qureshi

R is a robust prorammming language that allows for complex statistical computing and data visualization. This project
was developed using R Studio. The program reads in a data file from the web and outputs the statistical 
relationships of the data into a .txt file, while also creating graphs of these relationships 
that can be saved as .png files. View the project requirements and screenshots of the output below and 
download the source code [here](lis4369_p2.r).


### Project Requirements:


1. Code and run R file lis4369_p2.r, which contains various ways to display a dataset in R
2. Write the output of this code to a .txt file
3. Display .png plot files created by R code


#### Project Screenshots:

* Screenshot of *lis4369_p2.r*:

![Screenshot](img/output1.png)

* lis4369_p2.txt:

![Screenshot](img/output2.png)

* Screenshots of plots:

![Screenshot](img/plot_disp_and_mpg_1.png)
![Screenshot](img/plot_disp_and_mpg_2.png)

